


const app_id = "appZe9pQ0CqB2VrNH";
const app_key = "key09MTJx1IV6nl4X";

const word = document.querySelector('.word');
const partOfSpeech = document.querySelector('.part-of-speech');
const phoneticNotation = document.querySelector('.phonetic-notation');
const primaryDefinition = document.querySelector('.primary-definition');
const secondaryDefinition = document.querySelector('.secondary-definition');
const tertiaryDefinition = document.querySelector('.tertiary-definition');
const wordOrigin = document.querySelector('.word-origin');

const loader = document.querySelector('.loader');
const listenIcon = document.querySelector('.listen');

var tzoffset = (new Date()).getTimezoneOffset() * 60000;
// var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, 10);
var localISOTime = "2021-09-25";
// console.log(localISOTime);


var main = document.querySelector(".main");
// window.addEventListener("scroll", function () {
// 	console.log("scrolling!");
// 	var positionY = window.pageYOffset/16;
// 	document.body.style.backgroundPosition = "0 -" + positionY + "px";
// });

chrome.storage.local.get('lastSetDate', function(result) {

    if (result['lastSetDate'] != localISOTime) {

        chrome.storage.local.set({
            'lastSetDate': localISOTime
        }, function() {})

		const promiseOne = axios.get(
			"https://api.airtable.com/v0/" + app_id + "/Word%20Database?filterByFormula=DATESTR(%7BPublished+Date%7D)%3D%22" + localISOTime + "%22", {
				headers: {
					Authorization: "Bearer " + app_key
				}
			}
		).then(response => {
			// console.log(response.data.records);

			// Clear 2nd and 3rd definitions, not every record uses them
			chrome.storage.local.remove(["secondaryDefinition", "tertiaryDefinition"], function() {
				var error = chrome.runtime.lastError;
				if (error) {
					console.error(error);
				}
			})

			chrome.storage.local.set({
				'Word': response.data.records[0].fields['Word']
			}, function() {});

			chrome.storage.local.set({
				'partOfSpeech': response.data.records[0].fields['Part of Speech']
			}, function() {});

			chrome.storage.local.set({
				'phoneticNotation': response.data.records[0].fields['Phonetic Notation']
			}, function() {});

			chrome.storage.local.set({
				'primaryDefinition': response.data.records[0].fields['Primary Definition']
			}, function() {});

			if (response.data.records[0].fields['Secondary Definition'] != null) {
				chrome.storage.local.set({
					'secondaryDefinition': response.data.records[0].fields['Secondary Definition']
				}, function() {});
			}

			if (response.data.records[0].fields['Tertiary Definition'] != null) {
				chrome.storage.local.set({
					'tertiaryDefinition': response.data.records[0].fields['Tertiary Definition']
				}, function() {});
			}

			chrome.storage.local.set({
				'wordOrigin': response.data.records[0].fields['Word Origin']
			}, function() {});

			word.append(response.data.records[0].fields['Word']);
			partOfSpeech.append(response.data.records[0].fields['Part of Speech']);
			phoneticNotation.append(response.data.records[0].fields['Phonetic Notation']);
			primaryDefinition.append(response.data.records[0].fields['Primary Definition']);

			if (response.data.records[0].fields['Secondary Definition'] == null) {
				secondaryDefinition.remove();
			} else {
				secondaryDefinition.append(response.data.records[0].fields['Secondary Definition']);
			}

			if (response.data.records[0].fields["Tertiary Definition"] == null) {
				tertiaryDefinition.remove();
			} else {
				tertiaryDefinition.append(response.data.records[0].fields["Tertiary Definition"]);
			}

			wordOrigin.append(response.data.records[0].fields['Word Origin']);

			listenIcon.addEventListener('click', function (event) {
				var msg = new SpeechSynthesisUtterance(response.data.records[0].fields['Word']);
				listenIcon.classList.add("active");
				window.speechSynthesis.speak(msg);
				setTimeout(function(){
					listenIcon.classList.remove("active");
				}, 800);
			});

			// console.log("Data from api call has been stored");

		}).catch(error => {
			console.log(error);
		});

		const promiseTwo = axios.get(
			"https://api.unsplash.com/collections/79840/photos?client_id=9c89b71b1f64592d8c158c4e09c3b76207d2c066b97cb231396dbff515e7aec7"
		).then(response => {

			// console.log(response.data);
			const randomItem = response.data[Math.floor(Math.random() * response.data.length)];

			chrome.storage.local.set({
				'bgImage': randomItem.urls.full
			}, function() {});

			document.body.style.backgroundImage = 'url(' + randomItem.urls.full + ')';

		}).catch(error => {
			console.log(error);
		});

		Promise.allSettled([promiseOne, promiseTwo]).then(
			setTimeout(function(){
				loader.classList.remove("active");
			}, 300)
		);

    } else {

		chrome.storage.local.get(function(result) {
            word.append(result.Word);
            partOfSpeech.append(result.partOfSpeech);
            phoneticNotation.append(result.phoneticNotation);
            primaryDefinition.append(result.primaryDefinition);

            if (result.secondaryDefinition == null) {
                secondaryDefinition.remove();
            } else {
                secondaryDefinition.append(result.secondaryDefinition);
            }

            if (result.tertiaryDefinition == null) {
                tertiaryDefinition.remove();
            } else {
                tertiaryDefinition.append(result.tertiaryDefinition);
            }

			wordOrigin.append(result.wordOrigin);

			document.body.style.backgroundImage = 'url(' + result.bgImage + ')';

			listenIcon.addEventListener('click', function (event) {
				var msg = new SpeechSynthesisUtterance(result.Word);
				var voices = window.speechSynthesis.getVoices();
				msg.voice = voices[10];
				msg.voiceURI = 'native';
				msg.volume = 1; // 0 to 1
				// msg.rate = 8; // 0.1 to 10
				msg.pitch = 1; //0 to 2
				msg.lang = 'en-US';
				window.speechSynthesis.speak(msg);
				// Animate listen icon
				listenIcon.classList.add("active");
				setTimeout(function(){
					listenIcon.classList.remove("active");
				}, 800);
			});
        });

		setTimeout(function(){
			loader.classList.remove("active");
		}, 600);

    }

});

// Query what's in storage
// chrome.storage.local.get(function(result) {
//     console.log(result)
// })
